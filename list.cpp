	 // Semplice data stucture : lista concatenata non ordinata         
	   
	 #include<iostream>         
	   
	 using namespace std;         
	   
	 template <class T> class Node {         
	   
	 private:         
	   
	 	T* key;         
	   
	 	Node<T>* next;         
	   
	 public:         
	   
	 Node(T key){         
	   
	 		this->key=new T(key);         
	   
	 		next = NULL;         
	   
	 	}         
	   
	 	void setK(T key){*(this->key) = key;}         
	   
	 	void setN(Node<T>* next){this->next=next;}         
	   
	 	T getK(){return *key;}         
	   
	 	Node<T>* getN(){return next;}         
	   
	 };         
	   
	 template <class T> class List {         
	   
	 private:         
	   
	 	int n;// numero elementi         
	   
	 	Node<T> *head, *current;         
	   
	 	Node<T>* _search1(T key,Node<T> **p){         
	   
	 		Node<T> *x = head;         
	   
	 		*p = NULL;         
	   
	 		while(x!= NULL && x->getK()!=key){         
	   
	 			*p = x;         
	   
	 			x = x->getN();         
	   
	 		}         
	   
	 		if(x && x->getK()==key) return x;         
	   
	 		return NULL;         
	   
	 	}         
	   
	 public:         
	   
	 	List(){// costruisce una lista vuota         
	   
	 		n = 0;         
	   
	 		head = current = NULL;         
	   
	 	}         
	   
	 	List<T>& insert(T key){         
	   
	 		// inserimento in testa	         
	   
	 		Node<T> *y = new Node<T> (key);         
	   
	 		y->setN(head);         
	   
	 		head = y;         
	   
	 		n++;// aumento il numero di elementi dopo un inserimento         
	   
	 		return *this;         
	   
	 	}         
	   
	 	List<T>&  canc(T key){         
	   
	 		Node<T> *p;         
	   
	 		Node<T> *x= _search1(key, &p);         
	   
	 		if(x==NULL) return *this;         
	   
	 		Node<T> *y = x->getN();         
	   
	 		if(p==NULL) head =y;         
	   
	 		else p->setN(y);         
	   
	 		return *this;         
	   
	 	}         
	   
	 	int search1(T key){         
	   
	 		Node<T>* p;         
	   
	 		return (_search1(key,&p)!=NULL);         
	   
	 	}         
	   
	 	int isEmpty1(){return (n==0);}         
	   
	 	// iteratore         
	   
	 	void reset(){         
	   
	 		current = head;	         
	   
	 	}         
	   
	 	int hasNext(){         
	   
	 		return (current!=NULL);		         
	   
	 	}         
	   
	 	T* next(){         
	   
	 		if(!hasNext()) return NULL;         
	   
	 		T* tmp = new T(current->getK());         
	   
	 		current = current->getN();		         
	   
	 		return tmp;         
	   
	 	}         
	   
	 };         
	   

